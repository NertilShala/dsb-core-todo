/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Selvyn
 */
public class TagManipulatorTest
{
    private TagManipulator  tagManipulator;
    
    public TagManipulatorTest()
    {
    }
    
    @Before
    public void setUp()
    {
        tagManipulator = new TagManipulator();
    }

    @Test
    public  void    separate_out_empty_tag_list()
    {
        String  input = "";
        int expected = 1;
        String[] expected_tags = {""};
        String[] result = tagManipulator.parseString(input, ",\\ *");
        
        assertThat(result.length, is(expected) );
        assertArrayEquals(expected_tags, result);
    }

    @Test
    public  void    separate_out_tag_list_of_one() 
    {
    String  input = "shopping";   
    int expected_count = 1; 
    String[] expected_tags = {"shopping"}; 
    String[] result = tagManipulator.parseString(input, ",\\ *");
    
    assertThat(result.length, is(expected_count) ); 
    assertArrayEquals(expected_tags, result); 
    }
    
   @Test
   public  void    separate_out_tag_list_of_many()     {
    String  input = "shopping, music, jazz";
   }
}
